#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

NULL =

export DPKG_GENSYMBOLS_CHECK_LEVEL=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

include /usr/share/dpkg/architecture.mk

export QT_SELECT := qt5

QT_MODULE_VERSION = $(shell test -e .qmake.conf && ( cat .qmake.conf | grep MODULE_VERSION | sed -re 's/MODULE_VERSION\s*=\s*([^\s]+)\s*/\1/' ) || echo 5.0.0 )

%:
	dh $@ --with pkgkde_symbolshelper

execute_before_dh_auto_build:
	@echo "# Running syncqt.pl (QT_MODULE_VERSION=$(QT_MODULE_VERSION))"
	cd src/contacts/ && perl /usr/lib/qt5/bin/syncqt.pl -module QtContacts -version $(QT_MODULE_VERSION) -outdir $(CURDIR) -builddir $(CURDIR) $(CURDIR) && cd - 1>/dev/null \
	cd src/organizer/ && perl /usr/lib/qt5/bin/syncqt.pl -module QtOrganizer -version $(QT_MODULE_VERSION) -outdir $(CURDIR) -builddir $(CURDIR) $(CURDIR) && cd - 1>/dev/null \
	cd src/versit/ && perl /usr/lib/qt5/bin/syncqt.pl -module QtVersit -version $(QT_MODULE_VERSION) -outdir $(CURDIR) -builddir $(CURDIR) $(CURDIR) && cd - 1>/dev/null \
	cd src/versitorganizer/ && perl /usr/lib/qt5/bin/syncqt.pl -module QtVersitOrganizer -version $(QT_MODULE_VERSION) -outdir $(CURDIR) -builddir $(CURDIR) $(CURDIR) && cd - 1>/dev/null \

execute_after_dh_auto_clean:
	rm -f plugins/*/*.so
	rm -f qml/*/*.so

override_dh_auto_configure:
	dh_auto_configure -- QT_BUILD_PARTS+=tests

execute_after_dh_auto_build-indep:
	dh_auto_build -Smakefile -- docs

execute_after_dh_auto_install:
ifneq (,$(filter %-doc, $(shell dh_listpackages)))
	$(MAKE) INSTALL_ROOT=$(CURDIR)/debian/tmp install_docs
endif

	# Remove private stuff
	rm -rfv debian/tmp/usr/include/*/qt5/QtContacts/*/QtContacts/private/
	rm -rfv debian/tmp/usr/include/*/qt5/QtOrganizer/*/QtOrganizer/private/
	rm -rfv debian/tmp/usr/include/*/qt5/QtVersit/*/QtVersit/private/
	rm -rfv debian/tmp/usr/include/*/qt5/QtVersitOrganizer/*/QtVersitOrganizer/private/
	rm -fv $(CURDIR)/debian/tmp/usr/lib/*/qt5/mkspecs/modules/qt_lib_*_private.pri

	# drop unneeded .pro file
	rm -fv debian/tmp/usr/lib/*/qt5/examples/contacts/contacts.pro

	mkdir -p debian/tmp/usr/share/doc/qtpim5-examples/examples/organizer/
	cp -av examples/organizer/qmlorganizerlistview/ debian/tmp/usr/share/doc/qtpim5-examples/examples/organizer/
	cp -av examples/contacts/ debian/tmp/usr/share/doc/qtpim5-examples/examples/
	rm -rfv debian/tmp/usr/lib/x86_64-linux-gnu/qt5/examples/contacts/
	rm -fv debian/tmp/usr/share/doc/qtpim5-examples/examples/contacts/Makefile

	# Remove libtool-like files
	rm -f debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/*.la
	# Don't install the skeleton plugin
	rm -f debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/qt5/plugins/organizer/libqtorganizer_skeleton.so \
	    debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/cmake/Qt5Organizer/Qt5Organizer_QSkeletonOrganizerPlugin.cmake

ifneq (,$(filter %-doc, $(shell dh_listpackages)))
	mkdir -p debian/tmp/usr/share/doc/qtpim-doc-html/
	mv debian/tmp/usr/share/qt5/doc/qtcontacts debian/tmp/usr/share/doc/qtpim-doc-html/
	mv debian/tmp/usr/share/qt5/doc/qtorganizer debian/tmp/usr/share/doc/qtpim-doc-html/
	mv debian/tmp/usr/share/qt5/doc/qtversit debian/tmp/usr/share/doc/qtpim-doc-html/
	ln -s ../../doc/qtpim-doc-html/qtcontacts debian/tmp/usr/share/qt5/doc/qtcontacts
	ln -s ../../doc/qtpim-doc-html/qtorganizer debian/tmp/usr/share/qt5/doc/qtorganizer
	ln -s ../../doc/qtpim-doc-html/qtversit debian/tmp/usr/share/qt5/doc/qtversit
	cd debian/tmp/usr/share/doc/qtpim-doc-html/ && rdfind -makeresultsfile false -makesymlinks true . && cd - 1>/dev/null
	cd debian/tmp/usr/share/doc/qtpim-doc-html/ && symlinks -v -c -r . && cd - 1>/dev/null
endif

override_dh_auto_test:
	xvfb-run -a dh_auto_test --no-parallel -- LC_ALL=en_US.UTF-8 QML2_IMPORT_PATH=$(CURDIR)/qml QT_PLUGIN_PATH=$(CURDIR)/plugins LD_LIBRARY_PATH=$(CURDIR)/lib

include /usr/share/dpkg/pkg-info.mk

PKD   = $(word 1,$(abspath $(dir $(MAKEFILE_LIST))))
PKG   = $(DEB_SOURCE)
UVER  = $(shell echo $(DEB_VERSION) | cut -d "-" -f1)
DTYPE = +dfsg1
VER  ?= $(subst $(DTYPE),,$(UVER))

UURL = https://invent.kde.org/qt/qt/qtpim.git
UREV = $(shell echo $(VER) | cut -d"." -f3)

## http://wiki.debian.org/onlyjob/get-orig-source
.PHONY: get-orig-source
get-orig-source: $(PKG)_$(VER)$(DTYPE).orig.tar.xz $(info I: $(PKG)_$(VER)$(DTYPE))
	@

$(PKG)_$(VER)$(DTYPE).orig.tar.xz: $(info I: GIT Revision=$(UREV))
	$(if $(wildcard $(PKG)-$(VER)),$(error $(PKG)-$(VER) exist, aborting..))
	    @echo "# Downloading..."
	git clone $(UURL) $(PKG)-$(VER) \
	    || $(RM) -r $(PKG)-$(VER)
	cd $(PKG)-$(VER) \
	    && git checkout "$(UREV)" \
	&& ( echo "# Generating ChangeLog..." \
	        ; git --no-pager log --format="%ai %aN (%h) %n%n%x09*%w(68,0,10) %s%n" > ChangeLog \
	        ; touch -d "$$(git log -1 --format='%ci')" ChangeLog) \
	    && echo "# Setting times..." \
	    && for F in $$(git ls-tree -r --name-only HEAD); do touch --no-dereference -d "$$(git log -1 --format="%ai" -- $$F)" "$$F"; done \
	    && echo "# Cleaning-up..." \
	    && rm -Rfv doc/src/ \
	    && $(RM) -r .git .git* \
	    $(NULL)
	@echo "# Packing..."
	find -L "$(PKG)-$(VER)" -xdev -type f -print | sort \
	    | XZ_OPT="-6v" tar -caf "../$(PKG)_$(VER)$(DTYPE).orig.tar.xz" -T- --owner=root --group=root --mode=a+rX \
	    && $(RM) -r "$(PKG)-$(VER)" \
	    $(NULL)
