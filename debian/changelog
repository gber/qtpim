qtpim-opensource-src (5.0~git20201102.f9a8f0fc+dfsg1-3) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Apply multi-arch hints:
    + qtpim5-dev, qtpim5-examples: Add Multi-Arch: same.
    + qtpim5-doc, qtpim5-doc-html: Add Multi-Arch: foreign.
  * Remove constraints unnecessary since buster (oldstable):
    - Build-Depends: Drop versioned constraint on qml-module-qtquick2,
      qml-module-qttest, qtbase5-doc-html, qtbase5-private-dev,
      qtdeclarative5-doc-html, qtdeclarative5-private-dev and
      qttools5-dev-tools.

  [ Pino Toscano ]
  * Split override_dh_auto_build:
    - the execution of syncqt.pl in execute_before_dh_auto_build, so it is run
      before dh_auto_build in all the cases
    - turn the existing override_dh_auto_build-indep into
      execute_after_dh_auto_build-indep: this way it is only generated when
      building arch:all binaries too (saves build time otherwise)
  * Add the qdoc-qt5 indep build dependency, since now the documentation
    generation is unconditionally done if arch:all binaries are built.
  * Drop all the architecture filtering in qtpim5-doc-html.install, and
    qtpim5-doc.install: they are arch:all, on documentation is unconditionally
    built for them
    - drop the dh-exec shebang, and drop their execute bits, as they are no more
      needed now
    - drop the dh-exec build dependency, no more needed now
  * Switch from override_dh_auto_install to execute_after_dh_auto_install, to
    avoid explicitly calling the former
    - move all the documentation-related bits into checks for documentation
      packages being built, instead of checks for qdoc
  * Update standards version to 4.6.2, no changes needed.
  * Stop passing --fail-missing to dh_missing, as it is the default in compat
    13+.
  * Mark the xauth, and xvfb build dependencies as nocheck, as they are needed
    only by dh_auto_test.
  * Move the qtbase5-doc-html, and qtdeclarative5-doc-html build dependencies
    as indep build dependency, as they are needed only when building the
    documentation (in arch:all binaries).
  * Use execute_after_dh_auto_clean to avoid invoking dh_auto_clean manually.

  [ Guido Berhoerster ]
  * Remove cmake file belonging skeleton plugin. (Closes: #1012206)

 -- Pino Toscano <pino@debian.org>  Wed, 25 Jan 2023 08:00:55 +0100

qtpim-opensource-src (5.0~git20201102.f9a8f0fc+dfsg1-2) unstable; urgency=medium

  * Upload to unstable.

  * debian/rules:
    + Run unit tests with LC_ALL set to en_US.UTF-8. Required for calls to
      Qstring.localeAwareCompare() in tests/auto/contacts/qcontactsortorder/
      tst_qcontactsortorder.cpp.
  * debian/patches:
    + Drop 2002_disable-cmake-test.patch. Unit tests for CMake do work.
    + Drop 2099_disable_failing_tests.patch. Not required anymore if tests
      are run with en_US.UTF-8.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 07 Jun 2022 16:27:50 +0200

qtpim-opensource-src (5.0~git20201102.f9a8f0fc+dfsg1-1) experimental; urgency=medium

  * Upload to experimental, trigger auto-transition.

  * New upstream Git snapshot (f9a8f0fc914c040d48bbd0ef52d7a68eea175a98).
  * debian/{watch,rules}:
    + Adjust orig tarball retrieval for using kde/5.15 branch over at
      invent.kde.org.
  * debian/:
    + Rename libqt5<pim>5 to libqt5<pim>5a.
  * debian/patches:
    + Cherry-pick all Qt 5.15 compatible patches from upstream and rename
      some patches to match our patch naming scheme:
      Rename revert_module_version.patch -> 2000_revert_module_version.patch
      Rename Avoid-crash-while-parsing-vcards-from-different-threads.patch ->
        1002_Avoid-crash-while-parsing-vcards-from-different-threads.patch
      Rename disable_failing_tests.patch -> 2099_disable_failing_tests.patch
      Rename adapt_to_json_parser_change.patch ->
        1003_adapt_to_json_parser_change.patch
      Rename tests_big_endian.patch ->
        0024-Make-the-tests-pass-on-big-endian-systems.patch
      Add 0010-Use-QRegularExpression-instead-of-the-deprecated-QRe.patch
      Add 0011-Remove-usage-of-deprecated-QLatin1Literal.patch
      Add 0012-Fix-QList-from-QSet-conversions.patch
      Add 0013-Remove-usage-of-deprecated-QtAlgorithms.patch
      Add 0014-Add-missing-include.patch
      Add 0015-Remove-usage-of-deprecated-API-from-the-declarative-.patch
      Add 0016-More-QDateTime-QDate-QDate-startOfDay-fixes.patch
      Add 0017-Adjust-unit-test-to-account-for-QList-index-from-int.patch
      Add 0018-Remove-invalid-method-overload-which-confuses-the-QM.patch
      Add 0019-Specify-enum-flag-type-properly-in-unit-test.patch
      Add 0020-Remove-unused-method-in-unit-test.patch
      Add 0021-Update-qHash-methods-to-return-size_t-instead-of-uin.patch
      Add 0022-Mark-virtual-methods-with-override-keyword.patch
      Add 0023-Fix-calendardemo-example.patch
      Add 0025-Add-dependencies.yaml-for-CI.patch
      Add 0026-Bump-sha1s-in-dependencies.yaml.patch
      Add 0027-Fix-some-deprecated-QChar-constructor-issues-in-unit.patch
      Add 0028-Ensure-we-throw-away-the-BOM-in-the-qversitreader-te.patch
      Add 0029-Add-label-group-field-to-display-label-detail.patch
      Add 0030-Provide-interface-for-accessing-all-extended-metadat.patch
      Add 0031-Accessors-should-be-const.patch
      Add 0032-Enforce-detail-access-constraints-in-contact-operati.patch
      Add 0033-Set-PLUGIN_CLASS_NAME-in-.pro-files.patch
  * debian/control:
    + Bump DH compat level to version 13.
    + Bump Standards-Version: to 4.6.0. No changes needed.
    + Add B-Ds: rdfind, symlinks.
  * debian/rules:
    + Run syncqt.pl on-the-fly directly before the build (and don't ship the
      generated files as part of the orig tarball).
    + Set DPKG_GENSYMBOLS_CHECK_LEVEL to 1.
  * debian/*.symbols:
    + Update .symbols files.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/*.lintian-overrides:
    + Add some overrides.
  * debian/{rules,qtpim5-doc-html.install}:
    + Move documentation files to /usr/share/doc/ and symlink to its original
      location.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 03 May 2022 18:52:14 +0200

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-10) unstable; urgency=medium

  * debian/qtpim5-doc*.install:
    + Typo fix in mips64el architecture.
    + Add powerpc arch, it also has qt5-qtdoc.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 03 Jan 2022 21:04:35 +0100

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-9) unstable; urgency=medium

  * debian/copyright:
    + Drop trailing slash from Files-Excluded: pattern.
  * debian/{control,rules,qtpim5-doc*.install}:
    + Don't build and install API doc files with qtdoc if the architecture does
      not ship qt5-qtdoc. Use dhexec to adjust qtpim5-doc*.install files.
      (Closes: #1002970).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 03 Jan 2022 07:51:16 +0100

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-8) unstable; urgency=medium

  * Team upload.
  * Amend the patch to fix tests on big endian.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 31 Oct 2020 21:06:57 +0300

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-7) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/copyright: Use CGit URL in Source: field.

  [ Dmitry Shachnev ]
  * Add a patch to make the QVersitContactImporter tests pass with Qt 5.15
    (closes: #973436).
  * Add a patch to make the QContactFilter test pass on big endian systems.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 31 Oct 2020 17:43:06 +0300

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-6) unstable; urgency=medium

  * debian/rules:
    + Fix for removal of contacts.pro file.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Sep 2020 21:05:08 +0200

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-5) unstable; urgency=medium

  * debian/rules:
    + Drop unneeded .pro file that happens to appear on some non-amd64
      architecture builds. Resolves FTBFS on some architectures.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Sep 2020 20:29:53 +0200

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-4) unstable; urgency=medium

  * debian/control:
    + Move B-D-I packages to B-D. Fixes arch:any-only builds (as we now do the
      doc builds explicitly then, too).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Sep 2020 16:59:10 +0200

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-3) unstable; urgency=medium

  * debian/rules:
    + Explicitly build the docs during dh_auto_build. (Fixes arch:any builds).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Sep 2020 14:44:38 +0200

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-2) unstable; urgency=medium

  * debian/*.symbols:
    + Update symbols (for Qt 5.14 and others).
  * debian/rules:
    + Fix varying timestamps of on-the-fly created header files during orig
      tarball creation.
  * debian/patches:
    + Add 1001_fix-qtdatetime-null-comparison.patch. Partially reverts upstream
      commit 55287ea79625092ca7638fdb028a9c1f03ffd7ca. Required for Qt 5.14
      builds, was not required with Qt 5.12. (Closes: #969316).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Sep 2020 12:22:13 +0200

qtpim-opensource-src (5.0~git20190618.8fec622c+dfsg1-1) unstable; urgency=medium

  * Initial release to Debian (Closes: #955124).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 30 Mar 2020 17:08:23 +0200
